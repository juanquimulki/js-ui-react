import { render, screen } from '@testing-library/react';
import Orders from '../Components/Orders';
import Deposits from '../Components/Deposits';
import DogFacts from '../Components/DogFacts';

test('renders orders', () => {
  render(<Orders />);
  const title = screen.getByText(/recent orders/i);
  expect(title).toBeInTheDocument();
});

test('renders deposits', () => {
  render(<Deposits />);
  const title = screen.getByText(/recent deposits/i);
  expect(title).toBeInTheDocument();
});

test('renders dogfacts', () => {
  render(<DogFacts />);
  const title = screen.getByText(/dog facts/i);
  expect(title).toBeInTheDocument();
});
