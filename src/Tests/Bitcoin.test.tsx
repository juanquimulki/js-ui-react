import { fireEvent, getByTestId, render, screen, waitFor } from '@testing-library/react';
import App from '../App';

const { ResizeObserver } = window;

beforeEach(() => {
  //@ts-ignore
  delete window.ResizeObserver;
  window.ResizeObserver = jest.fn().mockImplementation(() => ({
    observe: jest.fn(),
    unobserve: jest.fn(),
    disconnect: jest.fn(),
  }));
});

afterEach(() => {
  window.ResizeObserver = ResizeObserver;
  jest.restoreAllMocks();
});

describe('Bitcoin', () => {
  const renderComponent = () => (render(<App />));

  test('renders bitcoin page', async () => {
    const { getByText } = renderComponent();

    fireEvent.click(getByText('Bitcoin'));

    await waitFor(() => {
      const title = getByText('Bitcoin Currency Exchange');
      expect(title).toBeInTheDocument();
    });
  });

  test('renders bitcoin cards', async () => {
    const { getByText } = renderComponent();

    fireEvent.click(getByText('Bitcoin'));

    await waitFor(() => {
      const usd = getByText('USD');
      const gbp = getByText('GBP');
      const eur = getByText('EUR');
      expect(usd).toBeInTheDocument();
      expect(gbp).toBeInTheDocument();
      expect(eur).toBeInTheDocument();

      const rate_usd = parseFloat(screen.getByTestId('rate-USD').innerHTML);
      const rate_gbp = parseFloat(screen.getByTestId('rate-GBP').innerHTML);
      const rate_eur = parseFloat(screen.getByTestId('rate-EUR').innerHTML);
      expect(rate_usd).toBeGreaterThan(0);
      expect(rate_gbp).toBeGreaterThan(0);
      expect(rate_eur).toBeGreaterThan(0);

      const desc_usd = screen.getByTestId('desc-USD');
      const desc_gbp = screen.getByTestId('desc-GBP');
      const desc_eur = screen.getByTestId('desc-EUR');
      expect(desc_usd).not.toBeEmptyDOMElement();
      expect(desc_gbp).not.toBeEmptyDOMElement();
      expect(desc_eur).not.toBeEmptyDOMElement();
    });
  });
})
