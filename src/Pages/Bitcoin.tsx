import React from 'react';
import Grid from '@mui/material/Grid';
import { Container } from '@mui/material';
import BitcoinCard from '../Components/BitcoinCard';
import { BitcoinResult } from '../Classes/BitcoinClass';

const BC_EXCHANGE_URL = "https://api.coindesk.com/v1/bpi/currentprice.json";

function BitcoinContent() {
  const [data, setData] = React.useState<BitcoinResult>(new BitcoinResult());

  React.useEffect(() => {
    requestData();
  }, []);

  const requestData = async () => {
    await fetch(BC_EXCHANGE_URL)
      .then(response => response.json())
      .then((response) => {
        setData(response);
      }).catch(() => {
        alert("Error fetching bitcoin data");
      });
  }

  return (
    <React.Fragment>
      <Container sx={{ m: 3, textAlign: 'right' }}>Updated: {data.time.updated}</Container>
      <Grid container spacing={3}>
        <Grid item xs={12} md={4} lg={4}>
          <BitcoinCard {...data.bpi.USD}></BitcoinCard>
        </Grid>
        <Grid item xs={12} md={4} lg={4}>
          <BitcoinCard {...data.bpi.GBP}></BitcoinCard>
        </Grid>
        <Grid item xs={12} md={4} lg={4}>
          <BitcoinCard {...data.bpi.EUR}></BitcoinCard>
        </Grid>
      </Grid>
      <Container sx={{ m: 5 }}>{data.disclaimer}</Container>
    </React.Fragment>
  );
}

export default function Bitcoin() {
  return <BitcoinContent />;
}