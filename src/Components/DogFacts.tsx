import * as React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Title from './Title';
import { DogFactsResult } from '../Classes/DogFactsClass';
import { Grid, Pagination } from '@mui/material';

const DOG_FACTS_URL = "http://127.0.0.1:8042/api/v1/dog-facts/";

export default function DogFacts() {
  const [data, setData] = React.useState<DogFactsResult>(new DogFactsResult());

  const requestData = async (param: string) => {
    await fetch(DOG_FACTS_URL + param)
      .then(response => response.json())
      .then((response) => {
        setData(response);
      }).catch(() => {
        alert("Error fetching dog facts data");
      });
  }
  
  React.useEffect(() => {
    requestData("");
  }, []);

  const handlePagination = (_event: React.ChangeEvent<unknown>, value: number) => {
    requestData("?page=" + value);
  };

  return (
    <React.Fragment>
      <Title>Dog Facts</Title>
      <Grid container justifyContent="flex-end">
        <Pagination count={data.last_page} onChange={handlePagination}  />
      </Grid>
      <Table size="small">
        <TableHead>
          <TableRow>
            <TableCell>#</TableCell>
            <TableCell>Fact</TableCell>
            <TableCell>Date Created</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {data.data.map((row) => (
            <TableRow key={row.id}>
              <TableCell>{row.id}</TableCell>
              <TableCell>{row.fact}</TableCell>
              <TableCell>{row.created_at}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </React.Fragment>
  );
}
