import { Card, CardContent, Typography } from '@mui/material';
import { BitcoinData } from '../Classes/BitcoinClass';

function BitcoinCardContent({ code, description, rate_float } : BitcoinData) {
  return (
    <Card variant='outlined'>
      <CardContent>
        <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
          <span>{code}</span>
        </Typography>
        <Typography variant="h5" component="div">
          <span data-testid={"rate-" + code}>{rate_float.toFixed(2)}</span>
        </Typography>
        <Typography variant="h6">
          <span data-testid={"desc-" + code}>{description}</span>
        </Typography>
      </CardContent>
    </Card>
  );
}

export default function BitcoinCard({ code, description, rate_float } : BitcoinData) {
  return <BitcoinCardContent code={code} description={description} rate_float={rate_float} />;
}
