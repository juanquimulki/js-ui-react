export class DogFactsResult {
    last_page: number;
    data: Array<DogFactsData>;

    constructor() {
        this.last_page = 0;
        this.data = [];
    }
}

export class DogFactsData {
    id: number;
    fact: string;
    created_at: string;

    constructor() {
        this.id = 0
        this.fact = "";
        this.created_at = "";
    }
}
