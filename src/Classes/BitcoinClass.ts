export class BitcoinResult {
    disclaimer: string;
    time: {
        updated: string,
    };
    bpi: {
        USD: BitcoinData,
        GBP: BitcoinData,
        EUR: BitcoinData,
    }    

    constructor() {
        this.disclaimer = "";
        this.time = { updated: "" };
        this.bpi = {
            USD: new BitcoinData(),
            GBP: new BitcoinData(),
            EUR: new BitcoinData(),
        };
    }
} 

export class BitcoinData {
    code: string;
    description: string;
    rate_float: number;

    constructor() {
        this.code = "";
        this.description = "";
        this.rate_float = 0;
    }
}
